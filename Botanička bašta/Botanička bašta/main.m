//
//  main.m
//  Botanička bašta
//
//  Created by Barbara Strugarevic on 7/13/16.
//  Copyright © 2016 Barbara Strugarevic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
