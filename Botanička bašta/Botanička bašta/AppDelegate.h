//
//  AppDelegate.h
//  Botanička bašta
//
//  Created by Barbara Strugarevic on 7/13/16.
//  Copyright © 2016 Barbara Strugarevic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end